﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
		private string name;

		public string Name
		{
			get { return name; }
			set { if (string.IsNullOrEmpty(name)) name = value; }
		}

		public string Street { get; set; }

		public string Postalcode { get; set; }

		public string City { get; set; }

		public int Id { get; set; }

		public static List<School> List { get; set; }

		public School(string name, string street, string postalcode, string city, int id)
		{
			Name = name;
			Street = street;
			Postalcode = postalcode;
			City = city;
			Id = id;
		}

		public static string ShowAll()
		{
			string output = "Lijst van scholen:\n";
			foreach (var school in List)
			{
				output += $"{school.Name}, {school.Street}, {school.City}, {school.Id} \n";
			}
			return output;
		}

		public string ShowOne()
		{
			string output = $"Gegevens van de school {this.Name}, {this.Street}, {this.City}, {this.Id}";
			return output;
		}
	}
}
