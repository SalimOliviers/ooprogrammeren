﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
	class Lecturer : Person
	{
		public static List<Lecturer> List { get; set; }

		public List<Course> Courses { get; set; }

		public Lecturer(string firstname, string lastname, DateTime birthday, int id, int schoolId, string contactnumber) : base(firstname, lastname, birthday, id, schoolId, contactnumber)
		{
			Courses = new List<Course>();
		}

		public static string ShowAll()
		{
			string output = "Lijst van lectoren:\n";
			foreach (var lecturer in List)
			{
				output += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.Birthday}, {lecturer.Id}, {lecturer.SchoolId}\n";
			}
			return output;
		}

		public override string ShowOne()
		{
			return $"Gegevens van de lector: {FirstName}, {LastName}, {Birthday}, {Id}, {SchoolId}";
		}

		public string ShowCourses()
		{
			string result =  $"Vakken die de lector geeft: \n";
			foreach (var course in Courses)
			{
				result += $"{course.Title} ({course.CalculateWorkload()}) ";
			}
			return result;
		}

		public override string GetNameTagText()
		{
			return $"(LECTOR) {this.FirstName} {this.LastName}, Contactnummer: {this.ContactNumber}";
		}
	}
}
