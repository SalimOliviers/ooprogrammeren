﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class LabCourse : Course
    {
        public byte StudyPoints { get; set; }

        public string Materials { get; set; }

        public LabCourse(string title, byte studypoints, string materials) : base(title)
        {
            StudyPoints = studypoints;
            Materials = materials;
        }

        public override uint CalculateWorkload()
        {
            return StudyPoints * (uint) 4;
        }
    }
}
