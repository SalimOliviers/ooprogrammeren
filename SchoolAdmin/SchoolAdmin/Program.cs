﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoolbeheer");
            School school1 = new School(
                "GO! BS de Spits",
                "Thonetlaan 106",
                "2050",
                "Antwerpen",
                1
                );
            
            School school2 = new School
            (
                "GO! Koninklijk Atheneum Deurne,",
                "Fr. Craeybeckxlaan 22",
                "2100",
                "Deurne",
                2
            );

            School.List = new List<School>
            {
                school1,
                school2
            };
            Console.WriteLine(School.ShowAll());

            Student student1 = new Student
            (
                "Mohamed",
                "El Farisi",
                DateTime.Parse("1987-12-06"),
                1,
                1,
                "+324 123 456 78"
            );

            Student student2 = new Student
            (
                "Sarah",
                "Jansens",
                DateTime.Parse("1991-10-21"),
                2,
                2,
                "+324 123 456 78"
            );

            Student student3 = new Student
            (
                "Bart",
                "Jansens",
                DateTime.Parse("1990-10-21"),
                3,
                2,
                "+324 123 456 78"
            );

            Student student4 = new Student
            (
                "Farah",
                "El Farisi",
                DateTime.Parse("1987-12-06"),
                4,
                1,
                "+324 123 456 78"
            );

            Student.List = new List<Student>
            {
                student1,
                student2,
                student3,
                student4
            };
            Console.WriteLine(Student.ShowAll());

            Lecturer lector1 = new Lecturer 
            (
                "Adem",
                "Kaya",
                DateTime.Parse("1976-12-01"),
                1,
                1,
                "+324 123 456 78"
            );

            Lecturer lector2 = new Lecturer
            (
                "Anne",
                "Wouters",
                DateTime.Parse("1968-04-03"),
                2,
                2,
                "+324 123 456 78"
            );

            Lecturer.List = new List<Lecturer>
            {
                lector1,
                lector2
            };

            Console.WriteLine(Lecturer.ShowAll());

            var administratie1 = new AdministrativeStaff
            (
                "Raul",
                "Jacob",
                DateTime.Parse("1985-11-01"),
                1,
                1,
                "+324 123 456 78"
            );

            AdministrativeStaff.List = new List<AdministrativeStaff>
            {
                administratie1
            };
            Console.WriteLine(AdministrativeStaff.ShowAll());

            var lijstPersonen = new List<Person>
            {
                student1,
                student2,
                student3,
                student4,
                lector1,
                lector2,
                administratie1
            };

            foreach (var personen in lijstPersonen)
            {
                Console.WriteLine(personen.GetNameTagText());
            }

            Course theoryCourse = new TheoryCourse("OO Programmeren", 3);
            Course seminar = new Seminar("Docker");
            lector1.Courses.Add(theoryCourse);
            lector1.Courses.Add(seminar);

            Console.WriteLine(lector1.ShowOne());
            Console.WriteLine(lector1.ShowCourses());

            Console.ReadLine();
        }
    }
}
