﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    abstract class Course
    {
        public string Title { get; set; }

        protected Course(string title)
        {
            Title = title;
        }

        public abstract uint CalculateWorkload();
    }
}
