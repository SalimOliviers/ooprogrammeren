﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff : Person
    {
        public static List<AdministrativeStaff> List { get; set; }

        public AdministrativeStaff(string firstname, string lastname, DateTime birthday, int id, int schoolId, string contactnumber) : base(firstname, lastname, birthday, id, schoolId, contactnumber)
        {

        }

        public static string ShowAll()
        {
            string output = "Lijst van administraties:\n";
            foreach (var administrativeStaff in List)
            {
                output += $"{administrativeStaff.FirstName}, {administrativeStaff.LastName}, {administrativeStaff.Birthday}, {administrativeStaff.Id}, {administrativeStaff.SchoolId}\n";
            }
            return output;
        }

        public override string ShowOne()
        {
            return $"Gegevens van de AdministrativeStaff: {FirstName}, {LastName}, {Id}, {SchoolId}";
        }

        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE) {this.FirstName} {this.LastName}, Contactnummer: {this.ContactNumber}";
        }
    }
}
