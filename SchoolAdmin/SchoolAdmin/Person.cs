﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
	abstract class Person
	{
		public string FirstName { get; set; }

		private string lastName;

		public string LastName
		{
			get { return lastName; }
			set { if (string.IsNullOrEmpty(lastName)) lastName = value; }
		}

		protected DateTime Birthday { get; set; }

		public int Id { get; set; }

		public int SchoolId { get; set; }

		protected string ContactNumber { get; set; }

		protected Person(string firstName, string lastName, DateTime birthday, int id, int schoolId, string contactnumber)
		{
			FirstName = firstName;
			LastName = lastName;
			Birthday = birthday;
			Id = id;
			SchoolId = schoolId;
			ContactNumber = contactnumber;
		}

		public abstract string ShowOne();

		public virtual string GetNameTagText()
		{
			return $"{this.FirstName} {this.LastName}";
		}

	}
}
