﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student : Person
    {
		public static List<Student> List { get; set; }

		public Student(string firstname, string lastname, DateTime birthday, int id, int schoolId, string contactnumber) : base(firstname, lastname, birthday, id, schoolId, contactnumber)
		{

		}

		public static string ShowAll()
		{
			string output = "Lijst van studenten:\n";
			foreach (var student in List)
			{
				output += $"{student.FirstName}, {student.LastName}, {student.Birthday}, {student.Id}, {student.SchoolId}\n";
			}
			return output;
		}

		public override string ShowOne()
		{
			return $"Gegevens van de student: {FirstName}, {LastName}, {Birthday}, {Id}, {SchoolId}";
		}
	}
}
