﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class DayOFWeekProgram
    {
        public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int dag = int.Parse(Console.ReadLine());
            Console.WriteLine("Welke maand?");
            int maand = int.Parse(Console.ReadLine());
            Console.WriteLine("Welke jaar?");
            int jaar = int.Parse(Console.ReadLine());

            DateTime datum = new DateTime(jaar, maand, dag);

            Console.WriteLine($"{datum.ToString("dd MMMM yyyy")} is een {datum.DayOfWeek}");
        }
    }
}
