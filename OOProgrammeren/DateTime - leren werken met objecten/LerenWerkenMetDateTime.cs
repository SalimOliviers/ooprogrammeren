﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Welke dag van de week? (H8-dag-van-de-week)");
            Console.WriteLine("2. Fracties van seconden al verlopen sinds 2000? (H8-ticks-sinds-2000)");
            Console.WriteLine("3. Schrikkeljaren tussen 1800 en 2020! (H8-schrikkelteller)");
            Console.WriteLine("4. Hoe lang duurt de opvulling van een array met 1 miljoen ints? (H8-simple-timing)");
            short choice = Convert.ToInt16(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    DayOFWeekProgram.Main();
                    break;
                case 2:
                    Ticks2000Program.Main();
                    break;
                case 3:
                    Schrikkelteller.Main();
                    break;
                case 4:
                    ArrayTimerProgram.Main();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
