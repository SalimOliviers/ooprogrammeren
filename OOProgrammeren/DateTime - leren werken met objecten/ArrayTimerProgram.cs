﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArrayTimerProgram
    {
        public static void Main()
        {
            DateTime startTime, endTime;
            Int64[] array1Mil = new Int64[100000];
            startTime = DateTime.Now;
            for (int i = 0; i < array1Mil.Length; i++)
            {
                array1Mil[i] = i + 1;
            }
            endTime = DateTime.Now;
            TimeSpan elapsedTimeInMilisec = endTime - startTime;
            Console.WriteLine($"Het duurt {elapsedTimeInMilisec.TotalMilliseconds} milliseconden om een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
