﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticks2000Program
    {
        public static void Main()
        {
            TimeSpan ticks = DateTime.Now - new DateTime(2000, 01, 01);
            Console.WriteLine($"Sinds 1 januari 2000 zijn er {ticks.TotalMilliseconds} ticks voorbijgegaan.");
        }
    }
}
