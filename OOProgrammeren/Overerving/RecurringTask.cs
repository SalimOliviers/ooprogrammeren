﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class RecurringTask : Task
    {
        public RecurringTask(string description, byte days): base(description)
        {
            Console.WriteLine($"Deze taak moet om de {days} dagen herhaald worden.");
        }

        public static void DemonstrateTasks()
        {
            List<Task> taken = new List<Task>();
            while (true)
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. een taak maken");
                Console.WriteLine("2. een terugkerende taak maken");
                Console.WriteLine("3. stoppen");
                int input = int.Parse(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        Console.WriteLine("Beschrijving van de taak?");
                        string taak = Console.ReadLine();
                        Task t1 = new Task(taak);
                        taken.Add(t1);
                        break;
                    case 2:
                        Console.WriteLine("Beschrijving van de taak?");
                        string ttaak = Console.ReadLine();
                        Console.WriteLine("Aantal dagen tussen herhaling?");
                        byte days = Convert.ToByte(Console.ReadLine());
                        RecurringTask rt1 = new RecurringTask(ttaak, days);
                        taken.Add(rt1);
                        break;
                    case 3:
                        Overerving.StartSubmenu();
                        break;
                    default:
                        Console.WriteLine("Ongeldige waarde");
                        break;
                }
            }
        }
    }
}
