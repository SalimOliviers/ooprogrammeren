﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Task
    {
        public string Description { get; set; }

        public Task(string description)
        {
            Description = description;
            Console.WriteLine($"Taak {Description} is aangemaakt.");
        }
    }
}
