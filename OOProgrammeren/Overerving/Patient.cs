﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Patient
    {
        public string Naam { get; set; }

        public int Verblijfsduur { get; set; }

        public Patient(string naam, int verblijfsduur)
        {
            Naam = naam;
            Verblijfsduur = verblijfsduur;
        }

        public virtual void ShowCost()
        {
            Console.WriteLine($"{Naam}, een gewone patiënt die {Verblijfsduur} uur in het ziekenhuis gelegen heeft, betaalt \u20AC{50 + (20 * Verblijfsduur)}");
        }
    }
}
