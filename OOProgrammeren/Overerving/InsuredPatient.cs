﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class InsuredPatient: Patient
    {
        public InsuredPatient(string naam, int duur) : base(naam,duur)
        {

        }

        public override void ShowCost()
        {
            Console.WriteLine($"{base.Naam}, een verzekerde patiënt die {base.Verblijfsduur} uur in het ziekenhuis gelegen heeft, betaalt \u20AC{50 + (20 * Verblijfsduur) - ((50 + (20 * Verblijfsduur)) * 10 / 100)}");
        }

        public static void DemonstratePatients()
        {
            Patient p1 = new Patient("Vincent", 12);
            p1.ShowCost();
            InsuredPatient ip1 = new InsuredPatient("Tim", 12);
            ip1.ShowCost();
        }
    }
}
