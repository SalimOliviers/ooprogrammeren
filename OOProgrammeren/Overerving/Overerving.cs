﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Overerving
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Oefening 1: H12 Een bestaande klasse uitbreiden via overerving / Oefening 4: H12 Dynamic dispatch");
            Console.WriteLine("2. Oefening 2: H12 Klassen met aangepaste constructor maken");
            Console.WriteLine("3. Oefening 3: H12 Ziekenhuis");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    WorkingStudent.DemonstrateWorkingStudent();
                    break;
                case 2:
                    RecurringTask.DemonstrateTasks();
                    break;
                case 3:
                    InsuredPatient.DemonstratePatients();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
