﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class WorkingStudent: Student
    {
        private byte workHours = 10;
            
        public byte WorkHours
        {
            get { return workHours; }
            set { if (value < 1) workHours = 1;
                else if (value > 20) workHours = 20;
                else workHours = value; }
        }

        public WorkingStudent(byte workHours, string naam, ClassGroups klas, int age, byte mc, byte mp, byte mwt) : base(naam, klas, age, mc, mp, mwt)
        {
            WorkHours = workHours;
        }

        public bool HasWorkToday()
        {
            int getal = new Random().Next(2);
            if (getal == 0) return true;
            else return false;
        }

        public override void OShowOverview()
        {
            base.OShowOverview();
            Console.WriteLine($"Statuut: werkstudent \nAantal werkuren per week: {WorkHours}");
        }

        public static void DemonstrateWorkingStudent()
        {
            Student s1 = new Student
            {
                Name = "Salim",
                Age = 21,
                Class = ClassGroups.EA1,
                MarkCommunication = 15,
                MarkProgrammingPrinciples = 20,
                MarkWebTech = 18
            };
            WorkingStudent ws1 = new WorkingStudent(15, "Mark", ClassGroups.EA2, 20, 18, 19, 17);
            List<Student> s = new List<Student> { s1 , ws1};
            foreach (var item in s)
            {
                item.OShowOverview();
            }
        }

    }
}
