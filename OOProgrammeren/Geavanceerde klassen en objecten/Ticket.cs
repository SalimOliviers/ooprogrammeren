﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticket
    {
        private static Random rnd = new Random();
        public byte Prize { get; set; }

        public Ticket()
        {
            Prize = Convert.ToByte(rnd.Next(1, 101));
        }

        public static void Raffle()
        {
            for (int i = 0; i < 10; i++)
            {
                Ticket tck = new Ticket();
                Console.WriteLine($"Waarde van het lotje: {tck.Prize}");
            }
        }
    }
}
