﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeavanceerdeKlassenEnObjecten
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Pokémons makkelijk aanmaken (h10-chaining)");
            Console.WriteLine("2. Pokémons makkelijk aanmaken (h10-pokebattlecount)");
            Console.WriteLine("3. Gemeenschappelijke kenmerken (h10-tombola)");
            Console.WriteLine("4. CSV");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokemon.ConstructPokemonChained();
                    break;
                case 2:
                    Pokemon.DemonstrateCounter();
                    break;
                case 3:
                    Ticket.Raffle();
                    break;
                case 4:
                    CSVDemo.Run();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
