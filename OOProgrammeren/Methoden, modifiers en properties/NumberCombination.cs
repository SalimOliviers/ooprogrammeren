﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class NumberCombination
    {
        private int number1;
        private int number2;

        public int Number1 { get => number1; set => number1 = value; }
        public int Number2 { get => number2; set => number2 = value; }

        public int Som()
        {
            return Number1 + Number2;
        }
        public int Verschil()
        {
            return Number1 - Number2;
        }
        public int Product()
        {
            return Number1 * Number2;
        }
        public double Quotient()
        {
            if (Number1 == 0 || Number2 == 0) { throw new ArgumentException("Deling door 0 is niet mogelijk!"); };
            return Convert.ToDouble(Number1) / Convert.ToDouble(Number2);
        }
        public static void Main()
        {
            NumberCombination pair1 = new NumberCombination();
            pair1.Number1 = 12;
            pair1.Number2 = 34;
            Console.WriteLine("Paar:" + pair1.Number1 + ", " + pair1.Number2);
            Console.WriteLine("Sum = " + pair1.Som());
            Console.WriteLine("Verschil = " + pair1.Verschil());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }
    }
}
