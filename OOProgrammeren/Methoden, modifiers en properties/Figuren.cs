﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Figuren
    {

        public static void Main()
        {
            Triangle tr = new Triangle();
            tr.Height = -1.0;
            tr.Height = 0.0;
            tr.Height = 1.0;
            tr.Base = 3.0;
            Console.WriteLine($"Een driehoek met een basis van {tr.Base}m en een hoogte van {tr.Height}m heeft een oppervlakte van {tr.Surface}m²");
            Rectangle rt = new Rectangle
            {
                Height = 1.5,
                Width = 2.2
            };
            Console.WriteLine($"Een rechthoek met een Breedte van {rt.Width}m en een hoogte van {rt.Height}m heeft een oppervlakte van {rt.Surface}m²");
        }
    }
}
