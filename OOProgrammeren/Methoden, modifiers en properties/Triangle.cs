﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Triangle
    {
        private double @base = 1.0;
        private double height = 1.0;

        public double Base
        {
            get => @base; set
            {
                if (value <= 0) { Console.WriteLine($"Het is verboden een basis van {value} in te stellen!"); } else { @base = value; }
            }
        }
        public double Height
        {
            get => height; set { if (value <= 0) { Console.WriteLine($"Het is verboden een hoogte van {value} in te stellen!"); } else { height = value; } }
        }
        public Triangle()
        {

        }
        public double Surface
        {
            get {return Math.Round(@base * height / 2, 1); }
            
        }
    }
}
