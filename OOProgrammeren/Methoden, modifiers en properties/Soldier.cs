﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Soldier
    {
        private int health;
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    health = value;
                }
            }
        }
        private static int damage;
        public static int Damage
        {
            get
            {
                return damage;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    damage = value;
                }
            }
        }
    }
}
