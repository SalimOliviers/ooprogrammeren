﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingProgram
    {

        // nu maken we onze randomgenerator *buiten* Main
        
        public static void Main()
        {
            BarkingDog dog1 = new BarkingDog();
            BarkingDog dog2 = new BarkingDog();
            dog1.Name = "Swieber";
            dog2.Name = "Misty";
            while (true)
            {
                Console.WriteLine(dog1.Bark());
                Console.WriteLine(dog2.Bark());
            }
        }
    }
}
