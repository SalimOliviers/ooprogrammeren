﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV3
    {
        private byte percentage;

        public byte Percentage
        {
            get => percentage; set
            {
                if (value >= 0 && value <= 100)
                {
                    percentage = value;
                }
                else
                {
                    Console.WriteLine("Percentage is niet geldig!");
                }
            } }

        public Honors Honors { get 
            {
                if (Percentage > 85)
                {
                    return Honors.GrootsteOnderscheiding;
                }
                else if (Percentage <= 85 && Percentage > 75)
                {
                    return Honors.GroteOnderscheiding;
                }
                else if (Percentage <= 75 && Percentage > 68)
                {
                    return Honors.Onderscheiding;
                }
                else if (Percentage <= 68 && Percentage > 50)
                {
                    return Honors.Voldoende;
                }
                else
                {
                    return Honors.Onvoldoende;
                }
            } 
        }
        public static void Main()
        {
            Console.WriteLine("Voer je resultaat in: (op 100)");
            ResultV3 resultaat = new ResultV3
            {
                Percentage = Convert.ToByte(Console.ReadLine())
            };
            Console.WriteLine(resultaat.Honors);
        }
    }
}
