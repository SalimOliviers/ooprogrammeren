﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Student
    {
        private List<Student> studenten = new List<Student>();
        public string Name { get; set; }
        public ClassGroups Class { get; set; }

        public double OverallMark { get { return Math.Round(Convert.ToDouble(markCommunication + markProgrammingPrinciples + markWebTech) / 3,1); } }

        private int age;

        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0 || value > 120)
                {
                    Console.WriteLine("Leeftijd is niet geldig!");
                }
                else
                {
                    age = value;
                } 
            }
        }

        private byte markCommunication;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if (value < 0 || value > 20)
                {
                    Console.WriteLine("resultaat is niet geldig!");
                }
                else
                {
                    markCommunication = value;
                }
            }
        }

        private byte markProgrammingPrinciples;

        public byte MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }
            set
            {
                if (value < 0 || value > 20)
                {
                    Console.WriteLine("resultaat is niet geldig!");
                }
                else
                {
                    markProgrammingPrinciples = value;
                }
            }
        }

        private byte markWebTech;

        public byte MarkWebTech
        {
            get { return markWebTech; }
            set
            {
                if (value < 0 || value > 20)
                {
                    Console.WriteLine("resultaat is niet geldig!");
                }
                else
                {
                    markWebTech = value;
                }
            }
        }

        public Student()
        {
        }

        public Student(string name, ClassGroups @class, int age, byte markCommunication, byte markProgrammingPrinciples, byte markWebTech)
        {
            Name = name;
            Class = @class;
            Age = age;
            MarkCommunication = markCommunication;
            MarkProgrammingPrinciples = markProgrammingPrinciples;
            MarkWebTech = markWebTech;
        }

        public void ShowOverview()
        {
            if (studenten.Count == 0)
            {
                Console.WriteLine("Geen studenten beschikbaar! maak een student aan!");
            }
            else
            {
                foreach (var item in studenten)
                {
                    Console.WriteLine($"{item.Name}, {item.age} \nKlas: {item.Class} \n \nCijferrapport:\n**********\nCommunicatie: {item.markCommunication,12}\nProgramming Principles: {item.markProgrammingPrinciples}\nWeb Technology: {item.markWebTech,10}\nGemiddelde: {item.OverallMark,14} \n");
                }
            }
        }
        public virtual void OShowOverview()
        {
            Console.WriteLine($"{Name}, {age} \nKlas: {Class} \n \nCijferrapport:\n**********\nCommunicatie: {markCommunication,12}\nProgramming Principles: {markProgrammingPrinciples}\nWeb Technology: {markWebTech,10}\nGemiddelde: {OverallMark,14} \n");
        }

        public void CreateStudent()
        {
            Student std = new Student();
            Console.WriteLine("Naam: ");
            std.Name = Console.ReadLine();
            Console.WriteLine("Leeftijd: ");
            std.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Klasgroep (kies uit): ");
            Console.WriteLine("1." + ClassGroups.EA1);
            Console.WriteLine("2." + ClassGroups.EA2);
            Console.WriteLine("3." + ClassGroups.EB1);
            if (int.Parse(Console.ReadLine()) == 1){ std.Class = ClassGroups.EA1; }
            else if(int.Parse(Console.ReadLine()) == 2) { std.Class = ClassGroups.EA2; }
            else if (int.Parse(Console.ReadLine()) == 3) { std.Class = ClassGroups.EB1; }
            else { Console.WriteLine("Geen geldige keuze"); }
            Console.WriteLine("Cijfer communicatie: ");
            std.MarkCommunication = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("Cijfer programmeren: ");
            std.markProgrammingPrinciples = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("Cijfer webtechnologie: ");
            std.MarkWebTech = Convert.ToByte(Console.ReadLine());
            studenten.Add(std);
        }

        public void EditStudent()
        {
            if (studenten.Count == 0)
            {
                Console.WriteLine("Geen studenten beschikbaar! maak een student aan!");
            }
            else
            {
                Console.WriteLine("Wat is de indexpositie van de student die je wil aanpassen?");

                int choiceIndex = int.Parse(Console.ReadLine());
                Console.WriteLine("Wat wil je aanpassen ?");
                Console.WriteLine("1. Naam");
                Console.WriteLine("2. Leeftijd");
                Console.WriteLine("3. Klasgroep");
                Console.WriteLine("4. Cijfer communicatie");
                Console.WriteLine("5. Cijfer programmeren");
                Console.WriteLine("6. Cijfer webtechnologie");
                int choiceEdit = int.Parse(Console.ReadLine());

                Console.WriteLine("Wat is de nieuwe waarde?");

                switch (choiceEdit)
                {
                    case 1:
                        studenten[choiceIndex].Name = Console.ReadLine();
                        break;
                    case 2:
                        studenten[choiceIndex].Age = int.Parse(Console.ReadLine());
                        break;
                    case 3:
                        if (studenten[choiceIndex].Class == ClassGroups.EA1)
                        {
                            Console.WriteLine("1." + ClassGroups.EA2);
                            Console.WriteLine("2." + ClassGroups.EB1);
                            if (int.Parse(Console.ReadLine()) == 1)
                            {
                                studenten[choiceIndex].Class = ClassGroups.EA2;
                            }
                            else
                            {
                                studenten[choiceIndex].Class = ClassGroups.EB1;
                            }
                        }else 
                        if (studenten[choiceIndex].Class == ClassGroups.EA2)
                        {
                            Console.WriteLine("1." + ClassGroups.EA1);
                            Console.WriteLine("2." + ClassGroups.EB1);
                            if (int.Parse(Console.ReadLine()) == 1)
                            {
                                studenten[choiceIndex].Class = ClassGroups.EA1;
                            }
                            else
                            {
                                studenten[choiceIndex].Class = ClassGroups.EB1;
                            }
                        }
                        else if (studenten[choiceIndex].Class == ClassGroups.EB1)
                        {
                            Console.WriteLine("1." + ClassGroups.EA1);
                            Console.WriteLine("2." + ClassGroups.EA2);
                            if (int.Parse(Console.ReadLine()) == 1)
                            {
                                studenten[choiceIndex].Class = ClassGroups.EA1;
                            }
                            else
                            {
                                studenten[choiceIndex].Class = ClassGroups.EA2;
                            }
                        }
                        break;
                    case 4:
                        studenten[choiceIndex].MarkCommunication = Convert.ToByte(Console.ReadLine());
                        break;
                    case 5:
                        studenten[choiceIndex].MarkProgrammingPrinciples = Convert.ToByte(Console.ReadLine());
                        break;
                    case 6:
                        studenten[choiceIndex].MarkWebTech = Convert.ToByte(Console.ReadLine());
                        break;
                    default:
                        Console.WriteLine("Geen geldige waarde!");
                        break;
                }
            }
        }

        public void DeleteStudent()
        {
            if (studenten.Count == 0)
            {
                Console.WriteLine("Geen studenten beschikbaar! maak een student aan!");
            }
            else
            {
                Console.WriteLine("Wat is de indexpositie van de te verwijderen student?");
                studenten.RemoveAt(int.Parse(Console.ReadLine()));
            }
        }

        public void GemiddeldeCijfer(int num)
        {
            int avg = 0;
            if (num == 5)
            {
                foreach (var item in studenten)
                {
                    avg += item.MarkCommunication;
                }
                Console.WriteLine($"Gemiddelde cijfer op comunicatie: {avg / studenten.Count}");
            }
            if (num == 6) 
            {
                foreach (var item in studenten)
                {
                    avg += item.MarkProgrammingPrinciples;
                }
                Console.WriteLine($"Gemiddelde cijfer op programmeren: {avg / studenten.Count}");
            }
            if (num == 7) 
            {
                foreach (var item in studenten)
                {
                    avg += item.MarkWebTech;
                }
                Console.WriteLine($"Gemiddelde cijfer op webtechnologie: {avg / studenten.Count}");
            }
        }

        public static void ExecuteStudentMenu()
        {
            Student stdList = new Student();

            while (true)
            {
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. gegevens van de studenten tonen");
                Console.WriteLine("2. een nieuwe student toevoegen");
                Console.WriteLine("3. gegevens van een bepaalde student aanpassen");
                Console.WriteLine("4. een student uit het systeem verwijderen");
                Console.WriteLine("5. Gemiddelde resultaat communicatie");
                Console.WriteLine("6. Gemiddelde resultaat programmeren");
                Console.WriteLine("7. Gemiddelde resultaat webtechnologie");
                Console.WriteLine("8. stoppen");

                short choice = Convert.ToInt16(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        stdList.ShowOverview();
                        break;
                    case 2:
                        stdList.CreateStudent();
                        break;
                    case 3:
                        stdList.EditStudent();
                        break;
                    case 4:
                       stdList.DeleteStudent();
                        break;
                    case 5:
                        stdList.GemiddeldeCijfer(choice);
                        break;
                    case 6:
                        stdList.GemiddeldeCijfer(choice);
                        break;
                    case 7:
                        stdList.GemiddeldeCijfer(choice);
                        break;
                    case 8:
                        ArraysEnKlassen.StartSubmenu();
                        break;
                    default:
                        Console.WriteLine("Geen geldige keuze");
                        break;
                }
            }
        }

        public static void Main()
        {
            Student student1 = new Student();
            student1.Class = ClassGroups.EA2;
            student1.Age = 21;
            student1.Name = "Joske Vermeulen";
            student1.MarkCommunication = 12;
            student1.MarkProgrammingPrinciples = 15;
            student1.MarkWebTech = 13;
            student1.ShowOverview();
        }
    }
}
