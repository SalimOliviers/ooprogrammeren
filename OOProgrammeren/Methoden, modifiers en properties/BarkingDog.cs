﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingDog
    {
        private static Random rng = new Random();
        public string Name { get; set; }
        private string breed;

        public string Breed
        {
            get { return breed; }
        }


        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return "RUFF!";
            }
            else if (Breed == "Wolfspitz")
            {
                return "AwawaWAF!";
            }
            else if (Breed == "Chihuahua")
            {
                return "ARF ARF ARF!";
            }
            // dit zou nooit mogen gebeuren
            // maar als de programmeur van Main iets fout doet, kan het wel
            else
            {
                return "Euhhh... Miauw?";
            }
        }

        public BarkingDog()
        {
            // deze code wordt uitgevoerd wanneer een nieuwe hond wordt aangemaakt
            // bepaal hier met de randomgenerator het ras van de hond
            int dog1BreedNumber = rng.Next(0, 3);
            int dog2BreedNumber = rng.Next(0, 3);
            if (dog1BreedNumber == 0)
            {
                breed = "German Shepherd";
            }
            else if (dog1BreedNumber == 1)
            {
                breed = "Wolfspitz";
            }
            else
            {
                breed = "Chihuahua";
            }
            if (dog2BreedNumber == 0)
            {
                breed = "German Shepherd";
            }
            else if (dog2BreedNumber == 1)
            {
                breed = "Wolfspitz";
            }
            else
            {
                breed = "Chihuahua";
            }
        }
    }
}
