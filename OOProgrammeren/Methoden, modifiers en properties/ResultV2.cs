﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {
        public byte Percentage { get; set; }
        public Honors ComputeHonors()
        {
            if (Percentage > 85)
            {
                return Honors.GrootsteOnderscheiding;
            }
            else if (Percentage <= 85 && Percentage > 75)
            {
                return Honors.GroteOnderscheiding;
            }
            else if (Percentage <= 75 && Percentage > 68)
            {
                return Honors.Onderscheiding;
            }
            else if (Percentage <= 68 && Percentage > 50)
            {
                return Honors.Voldoende;
            }
            else
            {
                return Honors.Onvoldoende;
            }

        }
        public static void Main()
        {
            Console.WriteLine("Voer je resultaat in: (op 100)");
            ResultV2 resultaat = new ResultV2
            {
                Percentage = Convert.ToByte(Console.ReadLine())
            };
            Console.WriteLine(resultaat.ComputeHonors());
        }
    }
}
