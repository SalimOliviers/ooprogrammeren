﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MethodenModifiersProperties
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Resultaat van punten behaald! (H8-RapportModule-V1)");
            Console.WriteLine("2. Resultaat van punten behaald! versie 2 (H8-RapportModule-V2)");
            Console.WriteLine("3. Som, Verschil, product en deling! (H8-Getallencombinatie)");
            Console.WriteLine("4. Oppervlakte berekenen! (H8-Figuren)");
            Console.WriteLine("5. Overview Student! (H8-Studentklasse)");
            Console.WriteLine("6. Soldiers ATTACK! (H8-uniform-soldiers)");
            Console.WriteLine("7. Math.DoubleAddTwo! (H8-utility-methode)");
            Console.WriteLine("8. ResultV3! (H8-RapportModule-V3)");
            Console.WriteLine("9. Bark! Bark! (H8-honden)");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    ResultV1.Main();
                    break;
                case 2:
                    ResultV2.Main();
                    break;
                case 3:
                    NumberCombination.Main();
                    break;
                case 4:
                    Figuren.Main();
                    break;
                case 5:
                    Student.Main();
                    break;
                case 6:
                    SoldierGame.Main();
                    break;
                case 7:
                    MathProgram.Main();
                    break;
                case 8:
                    ResultV3.Main();
                    break;
                case 9:
                    BarkingProgram.Main();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
