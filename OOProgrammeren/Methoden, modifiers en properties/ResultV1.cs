﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV1
    {
        public byte Percentage { get; set; }

        public void PrintHonors()
        {
            if (Percentage > 85)
            {
                Console.WriteLine("Grootste onderscheiding");
            }
            else if (Percentage <= 85 && Percentage > 75)
            {
                Console.WriteLine("Grote onderscheiding");
            }
            else if (Percentage <= 75 && Percentage > 68)
            {
                Console.WriteLine("onderscheiding");
            }
            else if (Percentage <= 68 && Percentage > 50)
            {
                Console.WriteLine("voldoende");
            }
            else
            {
                Console.WriteLine("onvoldoende");
            }

        }
        public static void Main()
        {
            Console.WriteLine("Voer je resultaat in: (op 100)");
            ResultV1 resultaat = new ResultV1();
            resultaat.Percentage = Convert.ToByte(Console.ReadLine());
            resultaat.PrintHonors();
        }
    }
}
