﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Rectangle
    {
        private double width = 1.0;
        private double height = 1.0;

        public double Width
        {
            get => width; set
            {
                if (value <= 0) { Console.WriteLine($"Het is verboden een breedte van {value} in te stellen!"); } else { width = value; }
            }
        }
        public double Height
        {
            get => height; set
            {
                if (value <= 0) { Console.WriteLine($"Het is verboden een hoogte van {value} in te stellen!"); } else { height = value; }
            }
        }

        public Rectangle()
        {

        }

        public double Surface
        {
            get { return Math.Round(width * height, 1); }
        }
    }
}
