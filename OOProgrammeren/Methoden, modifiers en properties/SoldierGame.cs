﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class SoldierGame
    {
        public static void Main()
        {
            Soldier soldier1 = new Soldier();
            soldier1.Health = 100;

            Soldier soldier2 = new Soldier();
            soldier2.Health = 99; // om maar te tonen dat dit mag verschillen per soldaat

            Soldier soldier3 = new Soldier();
            soldier3.Health = 98;
            Soldier.Damage = 20;
            // beeld je in dat de game wat verder loopt
            // nu volgt de upgrade
            Soldier.Damage *= 2;
        }
    }
}
