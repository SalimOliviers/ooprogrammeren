﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArraysEnKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. prijzen met foreach (h11-prijzen)");
            Console.WriteLine("2. speelkaarten (h11-speelkaarten)");
            Console.WriteLine("3. organiseren van studenten (h11-organizer)");
            Console.WriteLine("4. Oefening 5: hoger, lager");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Prijzen.AskForPrices();
                    break;
                case 2:
                    PlayingCard.Main();
                    break;
                case 3:
                    Student.ExecuteStudentMenu();
                    break;
                case 4:
                    PlayingCard.HogerLager();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
