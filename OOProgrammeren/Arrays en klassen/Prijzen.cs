﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Prijzen
    {
        public static void AskForPrices()
        {
            double[] prijzen = new double[20];

            Console.WriteLine("Gelieve 20 prijzen in te geven");
            double prijs;
            double gemiddelde = 0;
            for (int i = 0; i < prijzen.Length; i++)
            {
                prijs = int.Parse(Console.ReadLine());
                prijzen[i] = prijs;
                gemiddelde += prijzen[i];
            }
            foreach (var item in prijzen)
            {
                if (item >= 5.00) { Console.WriteLine(Math.Round(item, 2)); }
            }

            Console.WriteLine($"Het gemiddelde bedrag is {gemiddelde / prijzen.Length}");
        }
    }
}
