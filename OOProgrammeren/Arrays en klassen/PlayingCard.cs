﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class PlayingCard
    {
        private List<PlayingCard> boekKaarten = new List<PlayingCard>();
        public int Getal { get; set; }
        public Suites Kleur { get; set; }

        public PlayingCard(int getal, Suites kleur)
        {
            Getal = getal;
            Kleur = kleur;
        }

        public PlayingCard()
        {
        }

        public override string ToString()
        {
            return $"{Getal} {Kleur}";
        }
        public static List<PlayingCard> GenerateDeck(PlayingCard pc) {
            Suites a;
            for (int i = 0; i < 4; i++)
            {
                if (i == 0) { a = Suites.Clubs; }
                else if (i == 1) { a = Suites.Diamonds; }
                else if (i == 2) { a = Suites.Hearts; }
                else { a = Suites.Spades; };

                for (int j = 0; j < 13; j++)
                {
                  pc.boekKaarten.Add(new PlayingCard(j + 1, a));
                }
            }
            return pc.boekKaarten;
        }

        public static void ShowShuffledDeck(List<PlayingCard> cards)
        {
            for (int i = 0; i < 52; i++)
            {
                int index = new Random().Next(cards.Count);
                Console.WriteLine(cards[index].ToString());
                cards.RemoveAt(index);
            }
        }

        public static void ShowShuffledDeckV2(List<PlayingCard> cards)
        {
            int indexFirstCard = new Random().Next(cards.Count);
            PlayingCard previouscard = cards[indexFirstCard];
            cards.RemoveAt(indexFirstCard);
            for (int i = 0; i < 52; i++)
            {
                if (cards.Count != 0)
                {
                    int index = new Random().Next(cards.Count);
                    Console.WriteLine("de vorige kaart had waard " + previouscard.Getal);
                    
                    Console.WriteLine("Hoger (0), lager (1) of gelijk (2)");
                    int choice = int.Parse(Console.ReadLine());

                    while (choice != 0 || choice != 1 || choice != 2)
                    {
                        if (choice == 0)
                        {
                            if (cards[index].Getal > previouscard.Getal)
                            {
                                previouscard = cards[index];
                                cards.RemoveAt(index);
                               
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Spel voorbij!");
                                return;
                            }
                        }
                        else if (choice == 1)
                        {
                            if (cards[index].Getal < previouscard.Getal)
                            {
                                previouscard = cards[index];
                                cards.RemoveAt(index);
                                
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Spel voorbij!");
                                return;
                            }
                        }
                        else if (choice == 2)
                        {
                            if (cards[index].Getal == previouscard.Getal)
                            {
                                previouscard = cards[index];
                                cards.RemoveAt(index);
                                
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Spel voorbij!");
                                return;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Ongeldige keuze!!!!");
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Gewonnen!!!!!!");
                }
            }
        }

        public static void HogerLager()
        {
            PlayingCard pc = new PlayingCard();
            ShowShuffledDeckV2(GenerateDeck(pc));
        }

        public static void Main()
        {
            PlayingCard pc = new PlayingCard();
            ShowShuffledDeck(GenerateDeck(pc));
            
        }
    }
}
