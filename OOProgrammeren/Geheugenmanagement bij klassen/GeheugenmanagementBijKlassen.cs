﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeheugenmanagementBijKlassen
    {
        public static void StartSubmenu()
        {
            Console.WriteLine("Welke oefening wil je uitvoeren?");
            Console.WriteLine("1. Bootleg pokemon builder (H9-pokeattack/h10-pokeconstructie)");
            Console.WriteLine("2. COnscious pokemon (h9-consciouspokemon)");
            Console.WriteLine("3. RestoreHP (H9-pokevalueref)");
            Console.WriteLine("4. Fight! FIght! Fight (H9-pokefight)");

            short choice = Convert.ToInt16(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Pokemon.MakePokemon();
                    break;
                case 2:
                    Pokemon.TestConsciousPokemon();
                    break;
                case 3:
                    Pokemon.DemoRestoreHP();
                    break;
                case 4:
                    Pokemon.DemoFightOutcome();
                    break;
                default:
                    Console.WriteLine("Geen geldige keuze");
                    break;
            }
        }
    }
}
