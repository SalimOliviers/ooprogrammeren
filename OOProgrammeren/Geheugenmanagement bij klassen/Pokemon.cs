﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
	class Pokemon
	{
		private int maxHP;

		public int MaxHP
		{
			get { return maxHP; }
			set
			{
				if (value < 1 || value > 1000)
				{
					throw new ArgumentException("Waarde is ongelidg (tussen 1 - 1000)");
				}
				maxHP = value;
			}
		}

		private int hp;

		public int HP
		{
			get { return hp; }
			set
			{
				if (value < 0) { hp = 0; }
				if (value > maxHP) { hp = maxHP; }
				hp = value;
			}
		}

		public PokeSpecies PokeSpecies { get; set; }

		public PokeType PokeType { get; set; }

		public FightOutcome FightOutCome { get; set; }

		private static int grassCOunter, fireCounter, waterCounter, electricCounter = 0;

		public Pokemon()
		{
		}

		public Pokemon(int maxHP, int hP, PokeSpecies pokeSpecies, PokeType pokeType)
		{
			MaxHP = maxHP;
			HP = hP;
			PokeSpecies = pokeSpecies;
			PokeType = pokeType;
		}

		public Pokemon(int maxHP, PokeSpecies pokeSpecies, PokeType pokeType) : this(maxHP, maxHP / 2, pokeSpecies, pokeType)
		{

		}

		public void Attack()
		{
			if (PokeType == PokeType.Grass)
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!");
				Console.ForegroundColor = ConsoleColor.White;
			}
			if (PokeType == PokeType.Fire)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!");
				Console.ForegroundColor = ConsoleColor.White;
			}
			if (PokeType == PokeType.Water)
			{
				Console.ForegroundColor = ConsoleColor.Blue;
				Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!");
				Console.ForegroundColor = ConsoleColor.White;

			}
			if (PokeType == PokeType.Electric)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!");
				Console.ForegroundColor = ConsoleColor.White;

			}
		}

		public static void RestoreHP(Pokemon p)
		{
			p.HP = p.MaxHP;
		}

		public static FightOutcome FightOutcome(Pokemon poke1, Pokemon poke2, Random rnd)
		{
			Pokemon[] arrayFight = { poke1, poke2 };

			while (arrayFight[0].HP > 0 && arrayFight[1].hp > 0)
			{
				int randomPoke = rnd.Next(0, arrayFight.Length);
				arrayFight[randomPoke].Attack();
				arrayFight[(randomPoke + 1) % 2].HP -= rnd.Next(0, 21);
				if (arrayFight[(randomPoke + 1) % 2].HP > 0)
				{
					arrayFight[(randomPoke + 1) % 2].Attack();
					arrayFight[randomPoke].HP -= rnd.Next(0, 21);
				}
				else
				{
					if (arrayFight[randomPoke] == poke1)
					{
						return OOP.FightOutcome.WIN;
					}
					else
					{
						return OOP.FightOutcome.LOSS;
					}
				}
			}
			if (poke1.HP == 0)
			{
				return OOP.FightOutcome.LOSS;
			}
			else
			{
				return OOP.FightOutcome.WIN;
			}
		}

		public static void MakePokemon()
		{
			Pokemon poke = new Pokemon(20, 10, PokeSpecies.Bulbasaur, PokeType.Grass);
			poke.Attack();

			Pokemon poke2 = new Pokemon(20, 20, PokeSpecies.Charmander, PokeType.Fire);
			poke2.Attack();

			Pokemon poke3 = new Pokemon(20, 5, PokeSpecies.Pikachu, PokeType.Electric);
			poke3.Attack();

			Pokemon poke4 = new Pokemon(20, 3, PokeSpecies.Squirtle, PokeType.Water);
			poke4.Attack();
		}

		public static Pokemon FirstConsciousPokemon(Pokemon[] pokemon)
		{
			foreach (var item in pokemon)
			{
				if (item.hp >= 1)
				{
					return item;
				}
			}
			return null;
		}

		public static void TestConsciousPokemon()
		{
			Pokemon poke = new Pokemon();
			poke.MaxHP = 20;
			poke.HP = 0;
			poke.PokeSpecies = PokeSpecies.Bulbasaur;
			poke.PokeType = PokeType.Grass;
			Pokemon poke2 = new Pokemon();
			poke2.MaxHP = 20;
			poke2.HP = 0;
			poke2.PokeSpecies = PokeSpecies.Charmander;
			poke2.PokeType = PokeType.Fire;

			Pokemon poke3 = new Pokemon();
			poke3.MaxHP = 20;
			poke3.HP = 0;
			poke3.PokeSpecies = PokeSpecies.Pikachu;
			poke3.PokeType = PokeType.Electric;

			Pokemon poke4 = new Pokemon();
			poke4.MaxHP = 20;
			poke4.HP = 0;
			poke4.PokeSpecies = PokeSpecies.Squirtle;
			poke4.PokeType = PokeType.Water;

			Pokemon[] pokeArray = { poke, poke2, poke3, poke4 };

			if (FirstConsciousPokemon(pokeArray) == null)
			{
				Console.WriteLine("Al je Pokémon zijn KO! Haast je naar het Pokémon center.");
			}
			else
			{
				FirstConsciousPokemon(pokeArray).Attack();

			}
		}
		public static void DemoRestoreHP()
		{
			// aanmaken van array bewusteloze Pokemon van 4 soorten zoals eerder: zelf doen

			Pokemon poke = new Pokemon();
			poke.MaxHP = 150;
			poke.HP = 0;
			poke.PokeSpecies = PokeSpecies.Bulbasaur;
			poke.PokeType = PokeType.Grass;
			Pokemon poke2 = new Pokemon();
			poke2.MaxHP = 150;
			poke2.HP = 0;
			poke2.PokeSpecies = PokeSpecies.Charmander;
			poke2.PokeType = PokeType.Fire;

			Pokemon poke3 = new Pokemon();
			poke3.MaxHP = 150;
			poke3.HP = 2;
			poke3.PokeSpecies = PokeSpecies.Pikachu;
			poke3.PokeType = PokeType.Electric;

			Pokemon poke4 = new Pokemon();
			poke4.MaxHP = 150;
			poke4.HP = 20;
			poke4.PokeSpecies = PokeSpecies.Squirtle;
			poke4.PokeType = PokeType.Water;

			Pokemon[] pokemon = { poke, poke2, poke3, poke4 };

			for (int i = 0; i < pokemon.Length; i++)
			{
				Pokemon.RestoreHP(pokemon[i]);
			}
			for (int i = 0; i < pokemon.Length; i++)
			{
				Console.WriteLine(pokemon[i].HP);
			}
		}

		public static void DemoFightOutcome()
		{
			Pokemon bulbasaur = new Pokemon();
			bulbasaur.MaxHP = 20;
			bulbasaur.HP = 20;
			bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
			bulbasaur.PokeType = PokeType.Grass;
			Pokemon charmander = new Pokemon();
			charmander.MaxHP = 20;
			charmander.HP = 20;
			charmander.PokeSpecies = PokeSpecies.Charmander;
			charmander.PokeType = PokeType.Fire;
			Random ranGen = new Random();
			Console.WriteLine(FightOutcome(bulbasaur, charmander, ranGen));
		}

		public static void ConstructPokemonChained()
		{
			Pokemon poke1 = new Pokemon(20, PokeSpecies.Charmander, PokeType.Fire);
			Console.WriteLine($"De nieuwe {poke1.PokeSpecies} heeft maximum {poke1.MaxHP} HP en heeft momenteel {poke1.HP} HP");
		}

		public static void DemonstrateCounter()
		{
			Random rnd = new Random();
			for (int i = 0; i < 5; i++)
			{
				Pokemon pokemon;
				int getalType = rnd.Next(0, 4);
				for (int j = 0; j < rnd.Next(5, 11); j++)
					switch (getalType)
					{
						case 0:
							pokemon = new Pokemon(20, PokeSpecies.Charmander, PokeType.Fire);
							pokemon.Attack();
							fireCounter++;
							break;
						case 1:
							pokemon = new Pokemon(20, PokeSpecies.Bulbasaur, PokeType.Grass);
							pokemon.Attack();
							grassCOunter++;
							break;
						case 2:
							pokemon = new Pokemon(20, PokeSpecies.Pikachu, PokeType.Electric);
							pokemon.Attack();
							electricCounter++;
							break;
						default:
							pokemon = new Pokemon(20, PokeSpecies.Squirtle, PokeType.Water);
							pokemon.Attack();
							waterCounter++;
							break;
					}
			}
			Console.WriteLine($"Aantal aanvallen van Pokémon met type 'Fire' is {fireCounter}");
			Console.WriteLine($"Aantal aanvallen van Pokémon met type 'grass' is {grassCOunter}");
			Console.WriteLine($"Aantal aanvallen van Pokémon met type 'electric' is {electricCounter}");
			Console.WriteLine($"Aantal aanvallen van Pokémon met type 'water' is {waterCounter}");
		}
	}
}
