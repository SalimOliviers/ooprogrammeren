﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    enum PokeSpecies
    {
        
        Bulbasaur,
        Charmander,
        Squirtle,
        Pikachu

    }
    enum PokeType
    {
        Grass,
        Fire,
        Water,
        Electric
    }

    enum FightOutcome
    {
        WIN,
        LOSS,
        UNDECIDED
    }
}
